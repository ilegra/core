FROM maven:ibmjava-alpine
COPY . /app
RUN echo "cd /app/target && java -jar ilegra-test-core-0.1.0.jar" >> init.sh
RUN cd /app && mvn install
EXPOSE 8080
CMD ["/bin/bash", "init.sh"]


