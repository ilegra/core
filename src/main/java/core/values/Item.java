package core.values;

import java.util.Arrays;
import java.util.List;

public class Item {
	private final int id;
	private final int amount;
	private final double price;
	
	Item( List<String> coluns ) {
		if ( coluns.isEmpty() ) {
			this.id = 0;
			this.amount = 0;
			this.price = 0d;
		} else {
			this.id = Integer.parseInt(coluns.get(0).trim());
			this.amount = Integer.parseInt(coluns.get(1).trim());
			this.price = Double.parseDouble(coluns.get(2).trim());
		}
	}

	public int getId() {
		return id;
	}

	public int getAmount() {
		return amount;
	}

	public double getPrice() {
		return price;
	}
	
	public static Item fromLine(String line) {
		List<String> coluns = Arrays.asList(line.split("-"));
		return new Item(coluns);
	}
}
