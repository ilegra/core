package core.values;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.springframework.core.io.InputStreamResource;

public class Response {
	private final String filename;
	private final InputStreamResource bytes;

	public Response(String filename, String text) {
		this.filename = filename;
		InputStream inputStream = new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8));
		this.bytes = new InputStreamResource(inputStream);
	}

	public String getFilename() {
		return filename;
	}

	public InputStreamResource getBytes() {
		return bytes;
	}
}
