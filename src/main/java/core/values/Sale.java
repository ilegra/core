package core.values;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Sale {
	private final String saleId;
	private final List<Item> itens;
	private final String name;
	
	Sale( List<String> coluns ) {
		this.saleId = coluns.get(1).trim();
		String lineItens = coluns.get(2).trim();
		lineItens = lineItens.substring(1, lineItens.length() -1 );
		this.itens = Arrays.asList( lineItens.split(",") )
				.stream()
				.map(Item::fromLine)
				.collect(Collectors.toList());
		this.name = coluns.get(3);
	}

	public String getSaleId() {
		return saleId;
	}

	public List<Item> getItens() {
		return itens;
	}

	public String getName() {
		return name;
	}

	/**
	 * 003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato
	 */
	public static Sale fromLine(String line) {
		List<String> coluns = Arrays.asList(line.split("ç"))
				.stream()
				.map((column) -> column.trim())
				.collect(Collectors.toList());
		return new Sale(coluns);
	}
}
