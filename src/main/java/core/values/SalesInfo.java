package core.values;

import java.util.ArrayList;
import java.util.List;

public class SalesInfo {
	private final List<Salesman> salesmans = new ArrayList<>();
	private final List<Customer> customers = new ArrayList<>();
	private final List<Sale> sales = new ArrayList<>();
	
	void addCustumer(Customer customer) {
		this.customers.add(customer);
	}
	void addSales(Sale sale) {
		this.sales.add(sale);
	}
	void addSalesman(Salesman salesman) {
		this.salesmans.add(salesman);
	}
	
	public List<Salesman> getSalesmans() {
		return salesmans;
	}
	public List<Customer> getCustomers() {
		return customers;
	}
	public List<Sale> getSales() {
		return sales;
	}
}
