package core.values;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SalesInfoFactory {
	public static SalesInfo fromText(String text) {
		SalesInfo info = new SalesInfo();
		List<String> lines = Arrays.asList(text.split("\n"))
				.stream()
				.map((line) -> line.trim())
				.collect(Collectors.toList());
		
		lines.sort((p1, p2) -> p1.compareTo(p2));
		
		for (String line : lines) {
			if (line.startsWith("001")) {
				info.addSalesman(Salesman.fromLine(line));
			}
			if (line.startsWith("002")) {
				info.addCustumer(Customer.fromLine(line));
			}
			if (line.startsWith("003")) {
				info.addSales(Sale.fromLine(line));
			}
		}
		return info;
	}
}
