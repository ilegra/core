package core.values;

import java.util.Arrays;
import java.util.List;

public class Salesman {
	private final String CPF;
	private final String name;
	private final double salary;
	
	Salesman( List<String> coluns ) {
		this.CPF = coluns.get(1);
		this.name = coluns.get(2);
		this.salary = Double.parseDouble( coluns.get(3) );
	}
	
	public String getCPF() {
		return CPF;
	}
	
	public String getName() {
		return name;
	}
	
	public double getSalary() {
		return salary;
	}
	
	public static Salesman fromLine(String line) {
		List<String> coluns = Arrays.asList(line.split("ç"));
		return new Salesman(coluns);
	}
}
