package core.values;

import java.util.Arrays;
import java.util.List;

public class Customer {
	private final String CNPJ;
	private final String name;
	private final String businessArea;
	
	public Customer(List<String> coluns ) {
		this.CNPJ = coluns.get(1);
		this.name = coluns.get(2);
		this.businessArea = coluns.get(3);
	}
	
	public String getCNPJ() {
		return CNPJ;
	}
	public String getName() {
		return name;
	}
	public String getBusinessArea() {
		return businessArea;
	}
	
	public static Customer fromLine( String line ) {
		List<String> coluns = Arrays.asList(line.split("ç"));
		return new Customer(coluns);
	}

}
