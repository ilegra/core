package core.process;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import core.analysis.AnalyzeService;
import core.analysis.AnalyzeServiceImpl;
import core.values.Response;
import core.values.SalesInfo;
import core.values.SalesInfoFactory;

@Service
public class ProcessFileServiceImpl implements ProcessFileService {


    @Autowired
    public ProcessFileServiceImpl() {
    }

    @Override
    public Response processa(MultipartFile file) throws IOException {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        AnalyzeService service = new AnalyzeServiceImpl();
		String content = new String(file.getBytes(), "UTF-8");
    	SalesInfo info = SalesInfoFactory.fromText(content);
    	return new Response(filename, service.getString(info));
    }

}