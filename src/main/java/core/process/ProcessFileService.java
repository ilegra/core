package core.process;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import core.values.Response;

public interface ProcessFileService {

    Response processa(MultipartFile file) throws IOException;
}
