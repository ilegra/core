package core.analysis;

import java.util.stream.Collectors;

import core.values.SalesInfo;

/**
 * Amount of clients in the input file
 * @author David
 */
public class AmountOfClients implements Analyze {
	public final String name = "Amount of clients";

	@Override
	public Analysis analyze(SalesInfo info) {
		int size = info.getCustomers()
				.stream()
				.map(customers -> customers.getCNPJ())
				.collect(Collectors.toSet()) // Unique list
				.size();
		return new Analysis(this.name, size);
	}

}
