package core.analysis;

import java.util.stream.Collectors;

import core.values.SalesInfo;

/**
 * Amount of clients in the input file
 * @author David
 */
public class AmountOfSalesman implements Analyze {
	public final String name = "Amount of salesman";

	@Override
	public Analysis analyze(SalesInfo info) {
		int size = info.getSalesmans()
				.stream()
				.map(salesman -> salesman.getCPF())
				.collect(Collectors.toSet())
				.size();
		return new Analysis(this.name, size);
	}

}
