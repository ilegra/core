package core.analysis;

import core.values.SalesInfo;

public interface Analyze {
	Analysis analyze( SalesInfo info );
	
	public class Analysis {
		private final String desc;
		private final String value;
		Analysis( String desc, String value ) {
			this.desc = desc;
			this.value = value.toString();
		}
		Analysis(String desc, Number value) {
			this(desc, value.toString());
		}
		@Override
		public String toString() {
			return this.desc + ": " + this.value;
		}
	}
}
