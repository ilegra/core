package core.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import core.values.Sale;
import core.values.SalesInfo;

/**
 * Amount of clients in the input file
 * @author David
 */
public class WorstSalesman implements Analyze {
	public final String name = "Worst salesman";

	@Override
	public Analysis analyze(SalesInfo info) {
		List<SalesmanInfo> sales = info.getSales()
				.stream()
				.map(SalesmanInfo::fromSale)
				.collect(Collectors.toList());

		sales = mergeSalesmanInfo(sales);
		sales.sort((a, b) -> a.getValue().compareTo(b.getValue()));
		return new Analysis(this.name, sales.get(0).getName());
	}

	private List<SalesmanInfo> mergeSalesmanInfo(List<SalesmanInfo> sales) {
		sales.sort((a, b) -> a.getName().compareTo(b.getName()));
		List<SalesmanInfo> finalSales = new ArrayList<>();
		SalesmanInfo atual = null;
		for (SalesmanInfo proximo : sales) {
			if (atual == null) {
				atual = proximo;
				continue;
			}
			if (atual.getName().equals(proximo.getName())) {
				atual.accumulateValue(proximo.getValue());
			} else {
				finalSales.add(atual);
				atual = proximo;
			}
		}
		if (atual != null) {
			finalSales.add(atual);
		}
		return finalSales;
	}
	
	/**
	 * Inner class util apenas para calculo, por isso utiliza essa visibilidade
	 * @author david
	 */
	private static class SalesmanInfo {
		private final String name;
		private double value;
		
		public SalesmanInfo(Sale sale) {
			this.name = sale.getName();
			Optional<Double> value = sale.getItens()
					.stream()
					.map(item -> item.getAmount() * item.getPrice())
					.reduce((lastItem, presentItem) -> lastItem + presentItem);
			
			if ( value.isPresent() ) {
				this.value = value.get();
			} else {
				this.value = 0d;
			}
		}
		
		private String getName() {
			return name;
		}

		private Double getValue() {
			return value;
		}
		private void accumulateValue( double value ) {
			System.out.println(value);
			System.out.println(this.value);
			this.value += value;
		}

		private static SalesmanInfo fromSale( Sale sale ) {
			return new SalesmanInfo(sale);
		}
	}
}
