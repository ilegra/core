package core.analysis;

import java.util.Arrays;
import java.util.List;

import core.values.SalesInfo;

public class AnalyzeServiceImpl implements AnalyzeService {
	private final List<Analyze> analyzes = Arrays.asList(
			new AmountOfClients(),
			new AmountOfSalesman(),
			new MostExpensiveSale(),
			new WorstSalesman() );
	
	public String getString(SalesInfo info) {
		String retorno = "";
		for (Analyze analyze : analyzes) {
			retorno += analyze.analyze(info)+ "\n";
		}
		return retorno;
	}
}
