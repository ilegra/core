package core.analysis;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import core.values.Sale;
import core.values.SalesInfo;

/**
 * Amount of clients in the input file
 * @author David
 */
public class MostExpensiveSale implements Analyze {
	public final String name = "ID of the most expensive sale";

	@Override
	public Analysis analyze(SalesInfo info) {
		List<SaleInfo> sales = info.getSales()
				.stream()
				.map(SaleInfo::fromSale)
				.collect(Collectors.toList());
		sales.sort((a, b) -> new Double(b.value).compareTo(a.value));
		
		if ( sales.isEmpty() ) {
			return new Analysis(this.name, 0) ;
		}
		return new Analysis(this.name, sales.get(0).id) ;
	}
	
	/**
	 * Inner class util apenas para calculo, por isso utiliza essa visibilidade
	 * @author david
	 */
	private static class SaleInfo {
		private final String id;
		private final double value;

		public SaleInfo(Sale sale) {
			this.id = sale.getSaleId();
			Optional<Double> value = sale.getItens()
					.stream()
					.map(item -> item.getAmount() * item.getPrice())
					.reduce((lastItem, presentItem) -> lastItem + presentItem);
			
			if ( value.isPresent() ) {
				this.value = value.get();
			} else {
				this.value = 0d;
			}
		}
		private static SaleInfo fromSale( Sale sale ) {
			return new SaleInfo(sale);
		}
	}
}
