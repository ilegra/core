package core.analysis;

import core.values.SalesInfo;

public interface AnalyzeService {
	public String getString(SalesInfo info);
}
