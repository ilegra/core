package core;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import core.process.ProcessFileService;
import core.values.Response;

@Controller
public class FileUploadController {

    private final ProcessFileService processamento;

    @Autowired
    public FileUploadController(ProcessFileService processamento) {
        this.processamento = processamento;
    }

    @PostMapping("/")
	public ResponseEntity<InputStreamResource> handleFileUpload(@RequestParam("file") MultipartFile file,
		RedirectAttributes redirectAttributes) throws IOException {
		Response relatorio = processamento.processa(file);
		return ResponseEntity
				.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + relatorio.getFilename() + "\"")
				.body(relatorio.getBytes());

	}

}
