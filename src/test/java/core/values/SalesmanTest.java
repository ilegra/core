package core.values;

import org.junit.Test;

import core.values.Salesman;

public class SalesmanTest {

	@Test
	public void fromLine() {
		Salesman customer = Salesman.fromLine("001ç1234567891234çDiegoç50000");
		assert customer.getName().equals("Diego");
		assert customer.getSalary() == 50000d;
		assert customer.getCPF().equals("1234567891234");
	}
}
