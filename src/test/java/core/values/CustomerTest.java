package core.values;

import org.junit.Test;

import core.values.Customer;

public class CustomerTest {
	@Test
	public void fromLine() {
		Customer customer = Customer.fromLine("002ç2345675434544345çJose da SilvaçRural");
		assert customer.getName().equals("Jose da Silva");
		assert customer.getCNPJ().equals("2345675434544345");
		assert customer.getBusinessArea().equals("Rural");
	}
}
