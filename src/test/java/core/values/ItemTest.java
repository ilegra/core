package core.values;

import org.junit.Test;

import core.values.Item;

public class ItemTest {

	@Test
	public void fromLine() {
		Item item = Item.fromLine("3-40-3.10");
		assert item.getAmount() == 40;
		assert item.getPrice() == 3.10d;
		assert item.getId() == 3;
	}
}
