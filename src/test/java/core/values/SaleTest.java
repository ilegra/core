package core.values;

import org.junit.Test;

import core.values.Sale;

public class SaleTest {
	@Test
	public void fromLine() {
		Sale sale = Sale.fromLine("003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato");
		assert sale.getItens().size() == 3;
		assert sale.getName().equals("Renato");
		assert sale.getSaleId().equals("08");
	}
}
