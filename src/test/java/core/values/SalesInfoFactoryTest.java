package core.values;

import static org.junit.Assert.fail;

import org.junit.Test;

import core.analysis.AnalyzeService;
import core.analysis.AnalyzeServiceImpl;
import core.values.SalesInfo;
import core.values.SalesInfoFactory;

public class SalesInfoFactoryTest {
	
	@Test
	public void factory() {
		AnalyzeService analise = new AnalyzeServiceImpl();
		String text = 
				"001ç1234567891234çDiegoç50000\n" + 
				"001ç3245678865434çRenatoç40000.99\n" + 
				"002ç2345675434544345çJosedaSilvaçRural\n" + 
				"002ç2345675433444345çEduardoPereiraçRural\n" + 
				"003ç10ç[1-10-100,2-30-2.50,3-40-3.10]çDiego\n" + 
				"003ç08ç[1-34-10,2-33-1.50,3-40-0.10]çRenato";
		
		SalesInfo info = SalesInfoFactory.fromText(text);
		
		if ( info.getSales().size() != 2 ) {
			fail("O numero de sales deve ser igual a 2");
		}
		if ( info.getCustomers().size() != 2 ) {
			fail("O numero de custumers deve ser igual a 2");
		}
		if ( info.getSalesmans().size() != 2 ) {
			fail("O numero de salesmans deve ser igual a 2");
		}
		String result = "Amount of clients: 2\n" + 
				"Amount of salesman: 2\n" + 
				"ID of the most expensive sale: 10\n" + 
				"Worst salesman: Renato\n";
		
		assert analise.getString(info).equals(result);
	}

}
